- Komutlar Create CertManager

`kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.7.1/cert-manager.yaml`

`kubectl get po --namespace cert-manager`


Note: issuer.yaml da mail adresini gir.

`kubectl apply -f issuer.yaml`


- Cluster da oluşturacağın ingress ler için örnek ingress.yaml

Test için:

pod oluştur
`k run temp --image httpd`

service oluştur;
`k expose po temp --name temp --port 80`

route53 yada DNS'i nerede yönetiyorsak dns kaydı gir. hepapi.beratuyanik.com

örnek ingress'i oluştur.
`k apply -f ingress.yaml`

kontol için:
1-2 dakika bekle
`k get certificate` komutunda tls true ise tls certifikası oluşmuştur.