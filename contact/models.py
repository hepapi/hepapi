from email.headerregistry import Address
from django.db import models



class Contact(models.Model):

    address = models.CharField(max_length=100, blank=True, null=True, unique=False, default='')
    email = models.EmailField(max_length=50, blank=True, null=True, unique=False, default='')
    phone = models.CharField(max_length=13, blank=True, null=True, unique=False, default='')
    language = models.CharField(max_length=50, blank=True, null=True, unique=False, default='')

    
    

"""create contact us form"""
class ContactForm(models.Model):
    name = models.CharField(max_length=50, blank=True, null=True, unique=False, default='')
    email = models.EmailField(max_length=50, blank=True, null=True, unique=False, default='')
    phone = models.CharField(max_length=13, blank=True, null=True, unique=False, default='')
    message = models.CharField(max_length=500, blank=True, null=True, unique=False, default='')

    

"""create social media links"""
class SocialMedia(models.Model):
    facebook = models.CharField(max_length=150, blank=True, null=True, unique=False, default='')
    twitter = models.CharField(max_length=150, blank=True, null=True, unique=False, default='')
    instagram = models.CharField(max_length=150, blank=True, null=True, unique=False, default='')
    linkedin = models.CharField(max_length=150, blank=True, null=True, unique=False, default='')
    language = models.CharField(max_length=50, blank=True, null=True, unique=False, default='')
