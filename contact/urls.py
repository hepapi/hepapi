from rest_framework.routers import DefaultRouter
from applications.views import *
from django.db import router

from contact.models import ContactForm
from . import views
from django.urls import path, include







router = DefaultRouter()

router.register('socialmedia', views.SocialMediaModelViewSet)
router.register('contact', views.ContactModelViewSet)

urlpatterns = [
path('contactform', views.ContactForm.as_view()),
path('send_mail', views.contact_mail),
path('', include(router.urls))
 ]