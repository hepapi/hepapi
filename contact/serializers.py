from .models import *
from rest_framework.serializers import ModelSerializer


class ContactSerializer(ModelSerializer):
    class Meta:
        model = Contact
        fields = "__all__"



class ContactFormSerializer(ModelSerializer):
    class Meta:
        model = ContactForm
        fields = '__all__'


class SocialMediaSerializer(ModelSerializer):
    class Meta:
        model = SocialMedia
        fields = '__all__'
