from django.core.mail import EmailMultiAlternatives
from django.utils.html import strip_tags
from django.template.loader import render_to_string


class EmailManager:

    def __init__(self):
        self.to_email = None
        self.html_body = None
        self.email_subject = None

    def send_email(self):
        try:
            msg = EmailMultiAlternatives(
                self.email_subject,
                strip_tags(self.html_body),
                'berat.uyanik@feherconstruction.com',
                [self.to_email]
            )
            msg.attach_alternative(self.html_body, "text/html")
            msg.send(fail_silently=False)

            return True

        except Exception as e:
            print(e)
            return False


class ContactMail(EmailManager):

    def __init__(self, request):
        self.request = request

    def process(self):
        self.to_email = self.request.data["email"]
        self.email_subject = 'Feher Construction New Customer Request'
        self.context = {
            'name': self.request.data.get("name"),
            'email': self.request.data.get("email"),
            'phone': self.request.data["phone"],  # I only send the first contact info
            "message": self.request.data["message"],
        }
        self.html_body = render_to_string("contancFormMail.html", context=self.context)
        self.send_email()
