from rest_framework.exceptions import ValidationError

from feherApi.settings import FEHER_FROM_EMAIL
from .models import ContactForm as ContactFormModel
from .serializers import *
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import AllowAny, IsAdminUser
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from django.db.models import Q
from rest_framework.decorators import api_view
from rest_framework.generics import CreateAPIView
from .email import ContactMail
from .models import SocialMedia, Contact


@api_view(['POST'])
def contact_mail(request, *args, **kwargs):
    try:

        ContactMail(request).process()
        return Response({"message": "Email sent successfully"})

    except Exception as e:
        raise ValidationError(e)


class ContactModelViewSet(ModelViewSet):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer
    permission_classes = [IsAdminUser]

    @action(detail=False, methods=['GET'], name='HomePage List En')
    def listEn(self, request, *args, **kwargs):
        queryset = Contact.objects.filter(Q(language="English"))

        return Response(ContactSerializer(queryset, many=True).data, status=HTTP_200_OK)

    @action(detail=False, methods=['GET'], name='HomePage List Hu')
    def listHu(self, request, *args, **kwargs):
        queryset = Contact.objects.filter(Q(language="Hungarian"))

        return Response(ContactSerializer(queryset, many=True).data, status=HTTP_200_OK)

    def get_permissions(self):
        if self.action == 'list' or self.action == 'listEn' or self.action == 'listHu' or self.action == 'retrieve':
            permission_classes = [AllowAny]

        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]


class ContactForm(CreateAPIView):
    serializer_class = ContactFormSerializer
    queryset = ContactFormModel.objects.all()

    def post(self, request, *args, **kwargs):

        send = (self.create(request, *args, **kwargs)).data['id']
        send_obj = ContactFormModel.objects.filter(id=send).first()
        serializer = ContactFormSerializer(send_obj)

        subject = 'Müşteri'
        content = 'BERAT'
        name = request.data.get("name")
        phone = request.data.get("phone")
        email = serializer.data.get("email")
        message = serializer.data.get("message")

        try:

            ContactMail(request).process()
            return Response({"message": "Email sent successfully"})

        except Exception as e:
            raise ValidationError(e)


"""
This is the base model for all email sending.
"""


# it raises error because sendgrid suspends my account :( but I will send screenshots of previously successfully sent emails.


class SocialMediaModelViewSet(ModelViewSet):
    serializer_class = SocialMediaSerializer
    queryset = SocialMedia.objects.all()
    permission_classes = [IsAdminUser]

    @action(detail=False, methods=['GET'], name='HomePage List En')
    def listEn(self, request, *args, **kwargs):
        queryset = SocialMedia.objects.filter(Q(language="English"))

        return Response(SocialMediaSerializer(queryset, many=True).data, status=HTTP_200_OK)

    @action(detail=False, methods=['GET'], name='HomePage List Hu')
    def listHu(self, request, *args, **kwargs):
        queryset = SocialMedia.objects.filter(Q(language="Hungarian"))

        return Response(SocialMediaSerializer(queryset, many=True).data, status=HTTP_200_OK)

    def get_permissions(self):
        if self.action == 'list' or self.action == 'listEn' or self.action == 'listHu' or self.action == 'retrieve':
            permission_classes = [AllowAny]

        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]
