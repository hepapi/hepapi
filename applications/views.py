from rest_framework.viewsets import ModelViewSet
from .serializers import ApplicationSerializer
from .models import Application
from rest_framework.permissions import IsAdminUser, AllowAny
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from django.db.models import Q

# Create your views here.

class ApplicationModelViewSet(ModelViewSet):
    queryset = Application.objects.all()
    serializer_class = ApplicationSerializer
    permission_classes = [IsAdminUser]


    @action(detail=False, methods=['GET'], name='Application List En')
    def listEn(self, request, *args, **kwargs):
        queryset = Application.objects.filter(Q(language="English"))

        return Response(ApplicationSerializer(queryset, many=True).data, status=HTTP_200_OK)

    @action(detail=False, methods=['GET'], name='Application List Hu')
    def listHu(self, request, *args, **kwargs):
        queryset = Application.objects.filter(Q(language="Hungarian"))

        return Response(ApplicationSerializer(queryset, many=True).data, status=HTTP_200_OK)

    def get_permissions(self):
        if self.action == 'list' or self.action == 'listEn' or self.action == 'listHu' or self.action == 'retrieve':
            permission_classes = [AllowAny]

        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]
