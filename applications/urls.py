from rest_framework.routers import DefaultRouter
from .views import ApplicationModelViewSet
from django.urls import path, include

router = DefaultRouter()

router.register('application', ApplicationModelViewSet)

urlpatterns = [

    path('', include(router.urls))
]
