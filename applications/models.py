from django.db import models


class Application(models.Model):
    title = models.CharField(max_length=50, blank=True, null=True, unique=False, default='')
    image = models.FileField(blank=True, null=True, unique=False)
    description = models.CharField(max_length=3000, blank=True, null=True, unique=False, default='')
    language = models.CharField(max_length=50, blank=True, null=True, unique=False, default='')
    link  = models.CharField(max_length=50, blank=True, null=True, unique=False, default='')



