from rest_framework.routers import DefaultRouter
from .views import HomePageModelViewSet, OurPoliciesModelViewSet, HeaderModelViewSet
from django.urls import path, include

router = DefaultRouter()

router.register('homepage', HomePageModelViewSet)
router.register('header', HeaderModelViewSet)
router.register('our-policies', OurPoliciesModelViewSet)

urlpatterns = [

    path('', include(router.urls))]
