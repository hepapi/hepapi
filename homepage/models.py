from django.db import models


# Create homepage model.
class Homepage(models.Model):
    title = models.CharField(max_length=50, blank=True, null=True, unique=False, default='')
    description = models.CharField(max_length=3000, blank=True, null=True, unique=False, default='')
    image = models.FileField(blank=True, null=True, unique=False)
    language = models.CharField(max_length=50, blank=True, null=True, unique=False, default='')

class Header(models.Model):
    title = models.CharField(max_length=50, blank=True, null=True, unique=False, default='')
    description = models.CharField(max_length=3000, blank=True, null=True, unique=False, default='')
    image = models.FileField(blank=True, null=True, unique=False)
    language = models.CharField(max_length=50, blank=True, null=True, unique=False, default='')


class OurPolicies(models.Model):
    title = models.CharField(max_length=51, blank=True, null=True, unique=False, default='')
    description = models.TextField(blank=True, null=True, unique=False, default='')
    image = models.FileField(blank=True, null=True, unique=False)
    language = models.CharField(max_length=50, blank=True, null=True, unique=False, default='')

