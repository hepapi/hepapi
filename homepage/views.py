from .models import Header, Homepage, OurPolicies
from rest_framework.viewsets import ModelViewSet
from .serializers import HomePageSerializer, OurPoliciesSerializer, HeaderSerializer
from rest_framework.permissions import IsAdminUser,AllowAny
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from django.db.models import Q


# Create your views here.


class HomePageModelViewSet(ModelViewSet):
    queryset = Homepage.objects.all()
    serializer_class = HomePageSerializer
    permission_classes = [IsAdminUser]

    @action(detail=False, methods=['GET'], name='HomePage List En')
    def listEn(self, request, *args, **kwargs):
        queryset = Homepage.objects.filter(Q(language="English"))

        return Response(HomePageSerializer(queryset, many=True).data, status=HTTP_200_OK)

    @action(detail=False, methods=['GET'], name='HomePage List Hu')
    def listHu(self, request, *args, **kwargs):
        queryset = Homepage.objects.filter(Q(language="Hungarian"))

        return Response(HomePageSerializer(queryset, many=True).data, status=HTTP_200_OK)

    def get_permissions(self):
        if self.action == 'list' or self.action == 'listEn' or self.action == 'listHu' or self.action == 'retrieve':
            permission_classes = [AllowAny]

        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]


class HeaderModelViewSet(ModelViewSet):
    queryset = Header.objects.all()
    serializer_class = HeaderSerializer
    permission_classes = [IsAdminUser]

    @action(detail=False, methods=['GET'], name='HomePage List En')
    def listEn(self, request, *args, **kwargs):
        queryset = Header.objects.filter(Q(language="English"))

        return Response(HeaderSerializer(queryset, many=True).data, status=HTTP_200_OK)

    @action(detail=False, methods=['GET'], name='HomePage List Hu')
    def listHu(self, request, *args, **kwargs):
        queryset = Header.objects.filter(Q(language="Hungarian"))

        return Response(HeaderSerializer(queryset, many=True).data, status=HTTP_200_OK)

    def get_permissions(self):
        if self.action == 'list' or self.action == 'listEn' or self.action == 'listHu' or self.action == 'retrieve':
            permission_classes = [AllowAny]

        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]

class OurPoliciesModelViewSet(ModelViewSet):
    queryset = OurPolicies.objects.all()
    serializer_class = OurPoliciesSerializer
    permission_classes = [IsAdminUser]

    @action(detail=False, methods=['GET'], name='OurPolicies List En')
    def listEn(self, request, *args, **kwargs):
        queryset = OurPolicies.objects.filter(Q(language="English"))

        return Response(OurPoliciesSerializer(queryset, many=True).data, status=HTTP_200_OK)

    @action(detail=False, methods=['GET'], name='OurPolicies List Hu')
    def listHu(self, request, *args, **kwargs):
        queryset = OurPolicies.objects.filter(Q(language="Hungarian"))

        return Response(OurPoliciesSerializer(queryset, many=True).data, status=HTTP_200_OK)

    def get_permissions(self):
        if self.action == 'list' or self.action == 'listEn' or self.action == 'listHu' or self.action == 'retrieve':
            permission_classes = [AllowAny]

        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]
