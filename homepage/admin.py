from django.contrib import admin
from .models import Header, Homepage, OurPolicies

admin.site.register(Homepage)
admin.site.register(OurPolicies)
admin.site.register(Header)
