from homepage.models import Header, Homepage, OurPolicies
from rest_framework.serializers import ModelSerializer


class HomePageSerializer(ModelSerializer):
    class Meta:
        model = Homepage
        fields = "__all__"

class HeaderSerializer(ModelSerializer):
    class Meta:
        model = Header
        fields = "__all__"


class OurPoliciesSerializer(ModelSerializer):
    class Meta:
        model = OurPolicies
        fields = "__all__"
