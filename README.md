
### Case içerisinde gönderilen applerden birisini tercih etmedim, arkadaşımın şirketi için developmentını benim yaptığım Django ile yaazılmış bir api servisini kullandım case için. Servis içinde api ve db oluşuyor.

1. Kubernetes ortamına uygulamayı deploy edebilmek için öncelikle projenin dockerize hale getirilmesi gerekmektedir.

                - Dockerfile dosyasını hazırladım.

                - Base image olarak  python:3.7.3 image yerine --platform=linux/amd64 python:3.7.3-slim seçtim. Çünkü slim ile oluşturduğum image nın boyutunun olabildiğince küçük olmasını tercih ettim.

                -  python:3.7.3 ile build alınca çıkan image boyutu 1.02 GB iken python:3.7.3-slim ile build alınca image boyutu 232 MB olarak çıkıyor.

2. Projeyi gitlab-ci kullanarak deploy etmeye karar verdiğim için .gitlab-ci.yml dosyasını hazırladım. 

                - Pipeline 6 stage den oluşmaktadır. Bunlar sonarqube-check, FileScan, build, ImageScan, push ve deploy stage leridir.

                - Sonarqube stage de statik olarak kod analizinin yapılmasını ve tarama sonucu çıkan sonuçları bir rapor oluşturması için düzenledim. main branch üzerinde ve sonar-scanner ın kurulu olduğu server da bulunan runner tag ini burada düzenledim. Pipeline çalıştırılmadan önce sonarqube üzerinde proje oluşturulmalıdır. Proje oluşturulması esnasında elde edilen token ve url gitlab üzerinde environment olarak girilmelidir. Ayrıca sonar-project.properties dosyası düzenlediğim şekilde bulunmalıdır.

                - FileScan ve ImageScan  steplerinde  ilk olarak kodu tarayıp bir güvenlik zaafiyeti var mı yok mu diye kontrol ediyorum burada önemli olan nokta ise tarama sonucunda CRITICAL bir hata var ise cicd sürecini -exit-code 1  vererek kesiyorum. Sonrasında kod base de bir problem yok ise build işlemminden sonra oluşan imagı docker.io repoma pushlamadan önce image'ı tarıyorum yine aynı şekilde CRITICAL hata arıyorum bu adımda da bir critical hata mevcut ise -exit-code 1  ile süreci kesip hataları gösteren bir html dosyası çıkartırıyorum. Bu dosyada  hatalar ve çözüm önerileri oluyor. OWAPS kurallarına göre detaylı bir inceleme oluyor.

                - Build stage de Dockerfile üzerinden build alma işlemi gerçekleştirilir. main branch üzerinde ve docker ın kurulu olduğu runner server ın tag ini burada kullanacak şekilde düzenledim. Ayrıca build alınan image ların versiyonlanarak saklanması için de sonuna pipeline id versiyonlanmasını ekledim.

                - Push stage de öncelikle docker.io üzerinde proje oluşturulmalıdır. Yine build in alındığı runner tag i kullanılarak ve main branch kullanılarak elde edilen image beratuyanik/hepapi:v$version-id a gidecek şekilde düzenledim. Gitlab üzerinden docker username ve password environmentları girilmelidir.

                - Deploy stage de yine main branch ve kubernetes ortamına deployment yapılacak runner tag i girilerek düzenleme yaptım. Kuberntes ortamına deployment ı helm paketi ile yapacağım için helm komutlarını script kısmında oluşturdum. Yorum satırana aldığım kısımda ise credentialları vault üzerinde sakladığım senaryo için gerekli script bulunmaktadır. Deploy stage de bulunan image içerisinde ise deployment esnasında gerekli olan kubectl, helm ve vals (vault için) bulunmaktadır.

3. Kubernetes ortamına deploy için gerekli dosyaları helm paketi altında oluşturdum. 

                - api ve postgres olmak üzere iki microservisim üzerinden hareketle helm i oluşturdum. deployment altında environment dizini altında bulunan prod values dosyasında gerekli değerleri girerek yine helm altında templates dizini altında bulunan her microservise özel klasörler altında yaml dosyalarını bulunmaktadır.

                - api servisi için deployment, service, ingress, pvc, configmap ve hpa dosyaları bulunmaktadır.

                - postgres servisi için deployment, service ve  pvc dosyaları bulunmaktadır. (pv dosyasını cluster içerisinde dinamik olarak çalıştırılmayı planladığım için ilave olarak oluşturmadım.)

                - Deployment esnasında vault un da kullanılması senaryosu için yine prod_values dosyasında env kısmında değerleri yorum olarak ref ile yazdım.
                


4. Bunların dışında terraform ile oluşturmuş olduğum terraform dosyasını https://github.com/brtuynk/terraform-aws-private-eks bu linkte bulabilirsin yine bu projeyi module haline getirip terraform registeride yayınlaşmıştım yaklaşık 900 indirme mevcut modulün linkinide yine buraya bırakıyorum https://registry.terraform.io/modules/brtuynk/private-eks/aws/latest