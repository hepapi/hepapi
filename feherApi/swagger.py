from drf_yasg import openapi

from drf_yasg.views import get_schema_view



schema_view = get_schema_view(

    openapi.Info(

        title="Feher Construction API Documentation",

        default_version='v1',

        description="An API Documentation for feherconstruction.com",

        terms_of_service="https://www.feherconstruction.com/",

        contact=openapi.Contact(email="info@feherconstruction.com"),

        license=openapi.License(name="Feher Construction License"),

    ),

    url='https://api.feherconstruction.com/swagger/',

    public=True

)