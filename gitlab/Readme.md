- Gitlab-Runner Kurulumu

- Gitlab Admin sayfasında Overview -> Runner bölümüne sağ üstter register runner'a tıkla ve token al kaydet. Bu token 'ı gitlab-runner-values.yaml da token parametresi olarak gir.

- Yine aynı values file da tag kısmına runner ı işaret edeceğin tag i gir.

Eğer runner da private bir docker image kullanılacaksa;
- cluster da bir docker-registery secret git
   `kubectl create secret docker-registry registry-harbor --docker-server=harbor.beratuyanik.com --docker-username=admin --docker-password=Berat2024!`
- bu secret adını values file ında imagepullsecret parametresine yaz.

- Helm ile install edilir.
`helm repo add gitlab https://charts.gitlab.io`

`helm install gitlab-runner -f values.yaml gitlab/gitlab-runner`
       
`kubectl create clusterrolebinding default-clusterrolebinding-gitlab --clusterrole=gitlab-runner --serviceaccount=default:default`