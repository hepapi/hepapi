from django.db import models


class Project(models.Model):
    title = models.CharField(max_length=50, blank=True, null=True, unique=False, default='')
    image = models.FileField(blank=True, null=True, unique=False)
    language = models.CharField(max_length=10, blank=True, null=True, unique=False, default='')


class Brand(models.Model):
    title = models.CharField(max_length=50, blank=True, null=True, unique=False, default='')
    image = models.FileField(blank=True, null=True, unique=False)
    language = models.CharField(max_length=10, blank=True, null=True, unique=False, default='')
