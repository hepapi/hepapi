from .serializers import ProjectSerializer, BrandSerializer
from .models import Project, Brand
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAdminUser,AllowAny
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from django.db.models import Q


# Create your views here.

class ProjectModelViewSet(ModelViewSet):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    permission_classes = [IsAdminUser]

    @action(detail=False, methods=['GET'], name='Project List En')
    def listEn(self, request, *args, **kwargs):
        queryset = Project.objects.filter(Q(language="English"))

        return Response(ProjectSerializer(queryset, many=True).data, status=HTTP_200_OK)

    @action(detail=False, methods=['GET'], name='Project List Hu')
    def listHu(self, request, *args, **kwargs):
        queryset = Project.objects.filter(Q(language="Hungarian"))

        return Response(ProjectSerializer(queryset, many=True).data, status=HTTP_200_OK)

    def get_permissions(self):
        if self.action == 'list' or self.action == 'listEn' or self.action == 'listHu' or self.action == 'retrieve':
            permission_classes = [AllowAny]

        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]


class BrandModelViewSet(ModelViewSet):
    queryset = Brand.objects.all()
    serializer_class = BrandSerializer
    permission_classes = [IsAdminUser]

    @action(detail=False, methods=['GET'], name='Project List En')
    def listEn(self, request, *args, **kwargs):
        queryset = Brand.objects.filter(Q(language="English"))

        return Response(BrandSerializer(queryset, many=True).data, status=HTTP_200_OK)

    @action(detail=False, methods=['GET'], name='Project List Hu')
    def listHu(self, request, *args, **kwargs):
        queryset = Brand.objects.filter(Q(language="Hungarian"))

        return Response(BrandSerializer(queryset, many=True).data, status=HTTP_200_OK)

    def get_permissions(self):
        if self.action == 'list' or self.action == 'listEn' or self.action == 'listHu' or self.action == 'retrieve':
            permission_classes = [AllowAny]

        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]