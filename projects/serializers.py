from rest_framework.serializers import ModelSerializer
from .models import Project, Brand


class ProjectSerializer(ModelSerializer):
    class Meta:
        model = Project
        fields = '__all__'


class BrandSerializer(ModelSerializer):
    class Meta:
        model = Brand
        fields = '__all__'
