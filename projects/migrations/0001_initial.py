# Generated by Django 3.2.15 on 2022-10-20 07:15

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Brand',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, default='', max_length=50, null=True)),
                ('image', models.FileField(blank=True, null=True, upload_to='')),
                ('language', models.CharField(blank=True, default='', max_length=10, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, default='', max_length=50, null=True)),
                ('image', models.FileField(blank=True, null=True, upload_to='')),
                ('language', models.CharField(blank=True, default='', max_length=10, null=True)),
            ],
        ),
    ]
