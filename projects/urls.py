from .views import ProjectModelViewSet, BrandModelViewSet
from django.urls import path, include
from rest_framework.routers import DefaultRouter

router = DefaultRouter()

router.register('project', ProjectModelViewSet)
router.register('brand', BrandModelViewSet)


urlpatterns = [

    path('', include(router.urls))
]
